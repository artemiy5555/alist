package premitiv;

public interface IList {

    void clear();
    int size();
    int get(int index);
    boolean add(int number);
    boolean add(int index, int number);
    boolean remove(int number);
    boolean removeByIndex(int index);
    boolean contains(int number);
    void print();
    int[] toArray();
    AList subList(int fromIdex, int toIndex);
    boolean removeAll(int[] arr);
    boolean retainAll(int[] arr);

}
