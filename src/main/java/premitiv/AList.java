package premitiv;

import java.util.Arrays;

public class AList implements IList {

    private Integer integers[];

    AList() {
        integers = new Integer[0];
    }

    public AList(int[] array){
  //        integers = IntStream.of(array).boxed().toArray(Integer []::new);
            integers = new Integer[array.length];
            for ( int i = 0; i < array.length; i++ ) integers[i] = array[i];
    }


    @Override
    public void clear() {
        integers = null;
    }

    @Override
    public int size() {
        return integers.length;
    }

    @Override
    public int get(int index) {
        System.out.println(integers[index]);
        return integers[index];
    }

    @Override
    public boolean add(int number) {//

        Integer[] integer = new Integer[ integers.length +1];
        int i = 0;
        for (Integer val:integers){
            integer[i] = val;
            i++;
        }
        integer[i] = number;
        integers = null;
        integers = integer;
        System.out.println(Arrays.toString(integers));
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        try {
            integers[index] = number;
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public boolean remove(int number) {
        boolean bool = false;
        for (int i = 0; i < integers.length; i++) {
            try {
            if (integers[i] == number){
                integers[i]=null;
                bool = true;
            }}catch (NullPointerException ignored){}
        }
        return bool;
    }

    @Override
    public boolean removeByIndex(int index) {
        try {
            integers[index] = null;
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public boolean contains(int number) {
        for (Integer integer : integers) {
            if (integer == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        StringBuilder str= new StringBuilder(" (\"[");
        for (Integer integer : integers) {
            if (integer != null) {
                str.append(integer).append(",");
            }
        }
        str.deleteCharAt(str.length()-1);
        str.append("]\")");
        System.out.println(str);
    }
    @Override
    public int[] toArray() {
        //return Arrays.stream(integers).mapToInt(i->i).toArray();
        int[] array = new int[integers.length];
        for ( int i = 0; i < integers.length; i++ ) {
            if (integers[i]!=null) {
                array[i] = integers[i];
            }
        }
        return array;
    }

    @Override
    public AList subList(int fromIdex, int toIndex) {
        AList aList = new AList();
        for ( int i = fromIdex; i < toIndex; i++ ) {
            aList.removeByIndex(i);
        }
        return aList;
    }

    @Override
    public boolean removeAll(int[] arr) {
        try {
            for (int i = 0; i < integers.length; i++)
                for (int value : arr) {
                    if (value == integers[i])
                        integers[i] = null;
                }
            return true;
        }
        catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public boolean retainAll(int[] arr) {
        try {
            for (int value : arr) {
                add(value);
            }
            return true;
        }catch (NullPointerException e){
            return false;
        }

    }

}
