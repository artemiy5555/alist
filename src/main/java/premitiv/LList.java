package premitiv;

public class LList implements IList{

    public LList() {
    }

    public LList(int arr[]) {//
        retainAll(arr);
    }

    private Node head;

    public LList(Node head) {//
        this.head = head;
    }

    private boolean isEmpty(){//
        return head == null;
    }


    public void clear() {//
        Node temp = head;

        while (temp != null) {
            temp = temp.next;
            head =head.next;
        }
    }

    public int size() {//

        int size =0;
        Node temp = head;

        while (temp != null) {
            temp = temp.next;
            size++;
        }
        return size;
    }

    public int get(int index) {//

            Node temp = head;

            int num=0;
            while (temp != null) {
                if(index == num){
                    return temp.val;
                }
                num++;
                temp = temp.next;
            }
            return 0;

    }

    public boolean add(int number) {//
        try {
            Node node = new Node(number);
            node.next = head;
            head = node;
        return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean add(int index, int number) {//
        try {
            Node node = new Node(number);
            Node temp = head;
            node.next = head;
            int num=0;
            while (temp != null) {
                if(index == num){
                    temp.val = number;
                }
                num++;
                temp = temp.next;
            }
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean remove(int number) {//
        Node cur = head;
        Node prev = head;

        while (cur.val != number){
            if (isEmpty()) {

            } else {
                prev = cur;
                cur = cur.next;

            }
            if(cur == head) {
                head = head.next;

            }
            else {
                prev.next = cur.next;
                return true;
            }
        }
        return false;

    }

    public boolean remove() {//
        head =head.next;
        return true;
    }

    public boolean removeByIndex(int index) {//не пашет
        try {
            Node temp = head;
            int num=0;
            while (temp != null) {
                if(index == num){
                    //temp = head;
                    System.out.println(temp.val);
                }
                num++;
                temp = temp.next;
            }
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean contains(int number) {//
        Node temp = head;

        while (temp != null) {
            if(temp.val == number){
                return true;
            }
            temp = temp.next;
        }
        return false;
    }

    public void print() {//
        Node temp = head;

        while (temp != null) {
            System.out.println(temp.val);
            temp = temp.next;
        }
    }

    public int[] toArray() {//
        Node temp = head;
        int[] array = new int[size()];
        int i=0;
        while (temp != null) {
            array[i] =temp.val;
            temp = temp.next;
            i++;
        }
        return array;
    }

    public AList subList(int fromIdex, int toIndex) {

        return null;
    }

    public boolean removeAll(int[] arr) {//
        try {
            for (int value : arr) {
                remove(value);
            }
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    public boolean retainAll(int[] arr) {
        for (int value : arr) {
            add(value);
        }
        return true;
    }
}
