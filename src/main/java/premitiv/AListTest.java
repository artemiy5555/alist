package premitiv;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class AListTest {


    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUp(){
        System.setOut(new PrintStream(output));
    }

    @Test
    public void clear() {
        AList aList = new AList();
        aList.add(10);
        aList.clear();
        Assert.assertEquals("",output.toString());
    }

    @Test
    public void size() {
        AList bList = new AList();
        Assert.assertEquals(12,bList.size());
    }

    @Test
    public void get() {
        AList aList = new AList();
        aList.add(10);
        Assert.assertEquals(10,aList.get(0));
    }

    @Test
    public void add() {
        AList aList = new AList();
        aList.add(10);
        aList.print();
        Assert.assertEquals(" (\"[10,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void add1() {
        AList aList = new AList();
        aList.add(1,10);
        aList.print();
        Assert.assertEquals(" (\"[null,10,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void remove() {
        AList aList = new AList();
        aList.add(1,10);
        aList.remove(10);
        aList.print();
        Assert.assertEquals(" (\"[null,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void removeByIndex() {
        AList aList = new AList();
        aList.add(1,10);
        aList.removeByIndex(1);
        aList.print();
        Assert.assertEquals(" (\"[null,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void contains() {
        AList aList = new AList();
        aList.add(10);
        Assert.assertEquals(true,aList.contains(10));
    }

    @Test
    public void print() {
        AList aList = new AList();
        aList.print();
        Assert.assertEquals(" (\"[null,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void toArray() {
        AList aList = new AList();
        aList.print();
        Assert.assertEquals("[I@4f8e5cde",aList.toArray());
    }

    @Test
    public void subList() {
        AList aList = new AList();
        aList.add(10);
        aList.add(10);
        aList.add(10);
        aList.add(10);
        aList.subList(0,3);
        aList.print();
        Assert.assertEquals(" (\"[null,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void removeAll() {
        AList aList = new AList();
        aList.add(10);
        int[] array = new int[]{10};
        aList.removeAll(array);
        aList.print();
        Assert.assertEquals(" (\"[null,null,null,null,null,null,null,null,null,null,null,null)\"]"+"\n",output.toString());
    }

    @Test
    public void retainAll() {
        AList aList = new AList();
        aList.add(10);
        int[] array = new int[]{10,20,30};
        aList.retainAll(array);
        aList.print();
        Assert.assertEquals( "[10,10,20,30,null,null,null,null,null,null,null,null)",output.toString());
    }

}
