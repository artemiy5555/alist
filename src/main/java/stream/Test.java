package stream;

import java.util.function.Function;
import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {

        Predicate<Integer> negative = i -> i > 1;
        Function<Integer, Integer> convert = x-> x*x;

        try {
            long i = new Stream().of(1, 2, 3, 2, 1, 2).map(convert).sorted().filter(negative).count();
            System.out.println(i);
        }catch (NullPointerException ignored){}

    }
}
