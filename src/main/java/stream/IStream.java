package stream;

import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

public interface IStream {

    IStream filter(Predicate<Integer> predicate);

    IStream map(Function<Integer, Integer> function);

    IStream sorted(); // дефолтная сортировка

    IStream limit(long limit); // количество сущностей которое нужно вывести

    Integer reduce(BinaryOperator<Integer> binaryOperator);

    long count();

    IStream of(Integer ... values);

    Optional<Integer> findFirst();

    List<Integer> collect();

}