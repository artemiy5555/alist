package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

import static stream.MergeSortList.merge_sort;

public class Stream implements IStream {

    private List<Integer> collection;

    private Predicate<Integer> predicate;

    private Function<Integer, Integer> function;


    private long limit;

    private final List<Command> commands = new ArrayList<>();

    private enum Command {
        filter, map, sort, limit
    }

    @Override
    public IStream of(Integer... values) {//
        this.collection = Arrays.asList(values);
        return this;
    }

    @Override
    public Optional<Integer> findFirst() {
        end();
        return Optional.ofNullable(collection.get(0));

    }

    @Override
    public List<Integer> collect() {//
        end();
        return collection;
    }

    @Override
    public IStream filter(Predicate<Integer> predicate) {//
        this.predicate = predicate;
        this.commands.add(Command.filter);
        return this;
    }

    @Override
    public IStream map(Function<Integer, Integer> function) {//
        this.function = function;
        this.commands.add(Command.map);
        return this;
    }

    @Override
    public IStream sorted() {//
        this.commands.add(Command.sort);
        return this;
    }

    @Override
    public IStream limit(long limit) {//
        this.limit = limit;
        this.commands.add(Command.limit);
        return this;
    }

    @Override
    public Integer reduce(BinaryOperator<Integer> binaryOperator) {//

        int temp = 0;
        if(binaryOperator.apply(1,2)== 2){
            temp =1;
        }
        if(binaryOperator.apply(4,2)== 2){
            temp =1;
        }

        for(int val : collection){
           temp = binaryOperator.apply(temp,val);
        }

        return temp;
    }

    @Override
    public long count() {
        end();
        return collection.size();
    }

    private void end(){
        for (Command command: commands){
            switch (command){
                case map:
                    map();
                    break;
                case sort:
                    sort();
                    break;
                case limit:
                    limit();
                    break;
                case filter:
                    filter();
                    break;
            }
        }
    }

    private void filter(){
        List<Integer> tempCollection = new ArrayList<>();
        for (int val : collection){
            if (predicate.test(val))
                tempCollection.add(val);
        }
        this.collection = tempCollection;
    }

    private void limit(){

        List<Integer> tempCollection = new ArrayList<>();

        for(int i = 0;i<limit;i++) {
            tempCollection.add(collection.get(i));
        }

        this.collection = tempCollection;

    }

    private void sort(){
        this.collection = merge_sort(collection);
    }

    private void map() {
        List<Integer> tempCollection = new ArrayList<>();

        for (int val : collection){
                tempCollection.add(function.apply(val));
        }

        this.collection = tempCollection;
    }
}
