package generic;

import java.util.Arrays;
import java.util.List;

public class AList<T> implements IList {

    private T[] array;

    AList(T[] array) {//
        this.array = array.clone();
    }

    AList() {//
        this.array = (T[]) new Object[0];
    }

    @Override
    public void clear() {//
        this.array = null;
    }

    @Override
    public int size() {//
        return this.array.length;
    }

    @Override
    public Object get(int index) {//
        return this.array[index];
    }

    @Override
    public boolean add(Object val) {//
        T[] temp;
        temp = (T[]) new Object[ array.length +1];
        int i = 0;
        for (T t:array){
               // System.out.println(t);
                    temp[i] = (T) t;
                    i++;
        }
        temp[i] = (T) val;
        array = null;
        array = temp;
        return true;
    }

    @Override
    public boolean add(int index, Object number) {//
        try {
            if (array.length <index){
                add(number);
                return true;
            }
            array[index] = (T) number;
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public void remove(Object number) {//
        for (int i = 0; i < array.length; i++) {
            try {
                if (array[i].equals(number)){
                    array[i]=null;
                }}catch (NullPointerException ignored){}
        }
        removeNull(1);
    }

    private void removeNull(int minus){//
        T[] temp;
        temp = (T[]) new Object[array.length-minus];
        int i = 0;
        for (T t:array){
            if (t != null) {
                System.out.println(t);
                temp[i] = (T) t;
                i++;
            }
        }
        //System.out.println(Arrays.toString(temp));
        this.array = null;
        this.array = temp;

    }

    @Override
    public void removeByIndex(int index) {//
            this.array[index] = null;
            removeNull(1);
    }

    @Override
    public boolean contains(Object number) {
        for (T ignored : array) {
            if (array.equals(number)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(array));
    }

    @Override
    public List<T> toArray() {
        return Arrays.asList(array);
    }

    @Override
    public List<T> subList(int fromIdex, int toIndex) {
        for ( int i = fromIdex; i < toIndex; i++ ) {
            removeByIndex(i);
        }
        return Arrays.asList(array);
    }

    @Override
    public boolean removeAll(List arr) {
        try {
            for (int i = 0; i < array.length; i++)
                for (Object t : arr) {
                    if (t.equals(array[i]))
                        array[i] = null;
                }
            removeNull(arr.size());
            return true;
        }
        catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public boolean retainAll(List arr) {
        try {
            for (Object value : arr) {
                add(value);
            }
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }
}
