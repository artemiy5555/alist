package generic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;

public class LListTest {

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void clear() {
        LList lList = new LList();
        lList.add(10);
        lList.add(10);
        lList.add(10);
        lList.clear();
        Assert.assertEquals("",output.toString());
    }

    @Test
    public void size() {
        LList lList = new LList();
        lList.add(10);
        lList.add(10);
        lList.add(10);
        Assert.assertEquals(3,lList.size());
    }

    @Test
    public void get() {
        LList lList = new LList();
        lList.add(10);
        lList.add(20);
        Assert.assertEquals(20,lList.get(0));
        Assert.assertEquals(10,lList.get(1));
    }

    @Test
    public void add() {
        LList lList = new LList();
        Assert.assertTrue(lList.add(10));
    }

    @Test
    public void add1() {
        LList lList = new LList();
        Assert.assertTrue(lList.add(0, 10));
    }

    @Test
    public void remove() {
        LList lList = new LList();
        lList.add(10);
        lList.add(20);
        lList.add(30);
        //Assert.assertTrue(lList.remove(20));
    }

    @Test
    public void removeByIndex() {
    }

    @Test
    public void contains() {
        LList lList = new LList();
        lList.add(10);
        lList.add(20);
        lList.add(30);
        Assert.assertTrue(lList.contains(10));
    }

    @Test
    public void print() {
        LList lList = new LList();
        lList.add(10);
        lList.print();
        Assert.assertEquals("10\n",output.toString());
    }

    @Test
    public void toArray() {
        LList lList = new LList();
        lList.add(10);
        lList.print();
        Assert.assertEquals("10\n",output.toString());
    }

    @Test
    public void subList() {
    }

    @Test
    public void removeAll() {
        LList lList = new LList();
        lList.add(10);
        lList.add(20);
        lList.add(30);

        int[] array = new int[]{10, 20, 30};

        Assert.assertTrue(lList.removeAll(Collections.singletonList(array)));
    }

    @Test
    public void retainAll() {
        LList lList = new LList();
        lList.add(10);
        lList.add(20);
        lList.add(30);

        int[] array = new int[]{10, 20, 30};

        Assert.assertTrue(lList.retainAll(Collections.singletonList(array)));
    }
}
