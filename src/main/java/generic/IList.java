package generic;

import java.util.List;

public interface IList<T> {

    void clear();
    int size();
    T get(int index);
    boolean add(T  val);
    boolean add(int index, T  number);
    void remove(T  number);
    void removeByIndex(int index);
    boolean contains(T  number);
    void print(); //=> выводит в консоль все значения значения в квадратных скобках и через запятую ("[1, 2, 3, 4, 5, 6]")
    List<T> toArray(); //=> приводит данные к массиву
    List<T> subList(int fromIdex, int toIndex);
    boolean removeAll(List arr);
    boolean retainAll(List arr);

}
