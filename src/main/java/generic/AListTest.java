package generic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class AListTest<T> {


    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUp(){
        System.setOut(new PrintStream(output));
    }

    @Test
    public void clear() {
        AList aList = new AList();
        aList.add(10);
        aList.clear();
        aList.print();
        Assert.assertEquals("null"+"\n",output.toString());
    }

    @Test
    public void size() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        Assert.assertEquals(2,aList.size());
    }

    @Test
    public void get() {
        AList aList = new AList();
        aList.add(10);
        Assert.assertEquals(10,aList.get(0));
    }

    @Test
    public void add() {
        AList aList = new AList();
        Assert.assertTrue(aList.add(10));
    }

    @Test
    public void add1() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        Assert.assertTrue(aList.add(100,10));
    }

    @Test
    public void remove() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        aList.remove(10);
        Assert.assertEquals("20\n", output.toString());
    }

    @Test
    public void removeByIndex() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        aList.remove(1);
        Assert.assertEquals("20\n", output.toString());
    }

    @Test
    public void contains() {
        AList aList = new AList();
        aList.add(10);
        Assert.assertTrue(aList.contains(10));
    }

    @Test
    public void print() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        aList.add(30);
        aList.add(40);
        aList.print();
        Assert.assertEquals("[10, 20, 30, 40]\n", output.toString());
    }

    @Test
    public void toArray() {
    }

    @Test
    public void subList() {
    }

    @Test
    public void removeAll() {
        AList aList = new AList();
        aList.add(10);
        aList.add(20);
        aList.add(30);
        aList.add(40);
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        arrayList.add(40);
        aList.removeAll(arrayList);
        aList.print();
        Assert.assertEquals("[]\n", output.toString());
    }

    @Test
    public void retainAll() {
        AList aList = new AList();
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        aList.retainAll(arrayList);
        aList.print();
        Assert.assertEquals("[10, 20, 30]\n", output.toString());
    }

}
