package generic;

class Node {

    Object val;
    Node next;

    public Node(Object val) {
        this.val = val;
    }
}