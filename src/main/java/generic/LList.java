package generic;

import java.util.Arrays;
import java.util.List;

public class LList<T> implements IList {

    LList() {
    }

    public LList(T t[]) {//
        retainAll(Arrays.asList(t));
    }

    private Node head;

    public LList(Node head) {//
        this.head = head;
    }

    private boolean isEmpty(){//
        return head == null;
    }


    @Override
    public void clear() {
        Node temp = head;

        while (temp != null) {
            temp = temp.next;
            head =head.next;
        }
    }

    @Override
    public int size() {
        int size =0;
        Node temp = head;

        while (temp != null) {
            temp = temp.next;
            size++;
        }
        return size;
    }

    @Override
    public Object get(int index) {
        Node temp = head;

        int num=0;
        while (temp != null) {
            if(index == num){
                return temp.val;
            }
            num++;
            temp = temp.next;
        }
        return null;
    }

    @Override
    public boolean add(Object val) {
        try {
            Node node = new Node(val);
            node.next = head;
            head = node;
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean add(int index, Object number) {
        try {
            Node node = new Node(number);
            Node temp = head;
            node.next = head;
            int num=0;
            while (temp != null) {
                if(index == num){
                    temp.val = number;
                }
                num++;
                temp = temp.next;
            }
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public void remove(Object number) {
        Node cur = head;
        Node prev = head;

        while (cur.val != number){
            if (isEmpty()) {

            } else {
                prev = cur;
                cur = cur.next;

            }
            if(cur == head) {
                head = head.next;

            }
            else {
                prev.next = cur.next;
                return;
            }
        }

    }

    @Override
    public void removeByIndex(int index) {
            Node temp = head;
            int num=0;
            while (temp != null) {
                if(index == num){
                    //temp = head;
                    System.out.println(temp.val);
                }
                num++;
                temp = temp.next;
            }

    }

    @Override
    public boolean contains(Object number) {
        Node temp = head;

        while (temp != null) {
            if(temp.val == number){
                return true;
            }
            temp = temp.next;
        }
        return false;
    }

    @Override
    public void print() {
        Node temp = head;
        System.out.print("( ");
        while (temp != null) {
            System.out.print(temp.val+" ");
            temp = temp.next;
        }
        System.out.print(")");
    }

    @Override
    public List toArray() {
        Node temp = head;
        T[] t = (T[]) new Object[size()];
        int i=0;
        while (temp != null) {
            t[i] = (T) temp.val;
            temp = temp.next;
            i++;
        }
        return Arrays.asList(t);
    }

    @Override
    public List subList(int fromIdex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(List arr) {
        try {
            for (Object value : arr) {
                remove(value);
            }
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    @Override
    public boolean retainAll(List arr) {
        for (Object value : arr) {
            add(value);
        }
        return true;
    }
}
